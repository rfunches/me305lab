'''@file Fibonacci.py
 This file will be used to calculate the Fibonacci number
 located at a user defined index. The program only accepts
 positive integers. The full code can be found in my ME305Lab
 repository here:
     
   https://bitbucket.org/rfunches/me305lab/src/master/Lab%201/Fibonacci.py  '''
 
import sys #used for sys.exit() to end program later

#Commented out Fibonacci sequence generator that uses recursion
#Not time efficient
# def fib (idx):
#     #quality assurance stuff
#     if idx==0:
#         return 0
#     elif idx==1:
#         return 1
#     else:
#         return(fib(idx-2)+fib(idx-1))



def fib (idx):
    '''Fibonacci sequence generator that uses a while loop. Returns the Fibonacci
    Number at the input index. Computed values are stored in an array to decrease
        computational complexity while loop control variable i loops up to idx.'''
    if idx==0: #returns 0 if asking for index 0 of the sequence
        return 0
    if idx==1: #returns 1 if asking for index 1 of the sequence
        return 1
    seq=[0, 1] #new list created with the first two Fibonacci Sequence values
    i=2 #loop iteration variable declaration
    while i<=idx: #loop will run until list has required number of indices for the requested value of the sequence
        seq.append(seq[i-2]+seq[i-1]) #append next Fibonacci number based on the previous two list inputs
        i=i+1 #increment loop control variable
    return seq[i-1] #return the last entry into the list


#below function used from the attached website: 
# 
def is_integer(n):
    '''This function is used to ensure that a passed string is able to be converted
    to an integer in the main method. This was used from the following link:
    https://note.nkmk.me/en/python-check-int-float/#:~:text=Check%20if%20a%20number%20is%20integer%20or%20decimal,3%20Check%20if%20numeric%20string%20is%20integer.%20'''
    try:
        float(n)
    except ValueError:
        return False
    else:
        return float(n).is_integer()
 
 


if __name__ == '__main__':
    intake=input('Please input the index of the Fibonacci Sequence that you would like     ')
    if (is_integer(intake))==False: #tests if the input can be converted to int from string
        print('Please enter an integer on the next run')
        sys.exit(0) #Ends program if unable to be turned into an int
    else:
        index=int(intake) #converts the string input to int
    if index < 0:
        print('Please enter a positive integer on the next run')
        sys.exit(0) #Ends program if negative
    else:
        print ('Calculating Fibonacci number at '
        'index n = {:}.'.format(index))
                  
        ans=fib(index) #calls fib
        print('At index '+str(index)+' in the Fibonacci Sequence, the value is '+str(ans)) #output to user


