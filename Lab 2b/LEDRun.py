'''
@file LEDRun.py

This file contains the LED class. After initializiation, this object is able to
perform run1(), which prints ON and OFF at a fixed rate, and run2(), which uses 
Pulse Width Modulation (PWM) in order to brighten an LED on my Nucleo according
to a sawtooth waveform, where a brightness above 100 causes the brightness to return 
to 1. The transitionTo1() method allows for transition between the on and off states
in run1().
'''
#pyb works with the pins on the board
import pyb

#utime is used to work with millisecond counter on board
import utime

class LED:
    '''
    @brief      A finite state machine to control the LED output of the Nucleo.
    @details    This class implements a finite state machine to control the
                operation of a fictitious lED in run1() and a real LED in run2().
    '''
    ##Constant defining Task 1 State 1-ON
    T1State1=1
    
    ##Constant defining Task 1 State 2-OFF
    T1State2=2
   
    
    
    def __init__(self,interval1,interval2):
        '''
        @brief            Creates a LED object.
        @param currstate1   An integer that determines the current state of the first task
        @param currstate2   An integer that determines The current state of the second task, acts as a loop control variable for the waveform
        @param interval1  An integer that declares the delay time in microseconds between runs of task1
        @param interval2  An integer that declares the delay time in microseconds between runs of task2
        
        '''
        ## An integer that determines the current state of the first task
        self.currstate1=1
        
        ## An integer that determines The current state of the second task, acts as a loop control variable for the waveform
        self.currstate2=1
        
        ## An integer that declares the delay time in microseconds between runs of task1
        self.interval1=interval1
        
        ## An integer that declares the delay time in microseconds between runs of task2
        self.interval2=interval2
        
        # The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        # print('Start time:'+str(self.start_time))
        
        # The "timestamp" for when task 1 should run next
        self.next_time1 = utime.ticks_add(self.start_time, self.interval1)
        # print('Next Time1:'+str(self.next_time1))
        
        # The "timestamp" for when task 2 should run next
        self.next_time2 = utime.ticks_add(self.start_time, self.interval2)
        
        #Initializes the pin that will be interacted with to use the LED on the board
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        self.tim2 = pyb.Timer(2, freq = 20000)
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
        
    def run1(self):
        self.curr_time = utime.ticks_ms() #current time
        # print('Current time1:'+str(self.curr_time))
        if utime.ticks_diff(self.curr_time,self.next_time1)>0: #runs the task if current time is past the next time
             if (self.currstate1==self.T1State1): #if currently in state 1, print ON and transition to state 2
                 print('ON')
                 self.transitionTo1(self.T1State2)
             elif (self.currstate1==self.T1State2): #else if currently in state 2, print OFF and transition to state 1
                 print('OFF')
                 self.transitionTo1(self.T1State1)
                 
                 
             # if the task is run, set the next time based on interval1
             self.next_time1 = utime.ticks_add(self.next_time1, self.interval1) 
       
       
            
        
    def run2(self):
        self.curr_time = utime.ticks_ms() #current time
        if utime.ticks_diff(self.curr_time, self.next_time2)>0: #runs the task if current time is past the next time
            #print(str(self.currstate2)) 
            if(self.currstate2>=100): #if the current pulse width percent is over 100 set to 1
                self.currstate2=1
            else: #increment pulse width percent by 5
                self.currstate2=self.currstate2+5
            
            self.t2ch1.pulse_width_percent(self.currstate2) #changes output of real LED
            
            # if the task is run, set the next time based on interval2
            self.next_time2 = utime.ticks_add(self.next_time2, self.interval2)
                
    
    def transitionTo1(self, newState):
        '''
        @brief      Updates the variable defining the next state to run for run1()
        '''
        self.currstate1=newState


# no delays(use interval)