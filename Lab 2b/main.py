'''
@file main.py

Main class that interacts with LEDRun.py. LED object is created and will loop 
through running the tasks at the specified intervals until user interrupts.
'''
# import pyb
import utime
from LEDRun import LED

#Interval that Task 1 will run at in microseconds
interval1=100

#Interval that Task 2 will run at in microseconds
interval2=100

#Declare an LED object and pass the intervals
LD2=LED(interval1,interval2)

while True: #run the two tasks endlessly
    LD2.run1()
    LD2.run2()