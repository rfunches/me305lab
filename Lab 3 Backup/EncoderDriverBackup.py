# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 16:02:50 2020

@author: rjfun
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 13:50:29 2020

@author: rjfun
"""


class EncoderRunner:
  
    
  
    def __init__(self,enc,interval1):
  
        ## An encoder object that is to be run by this object
        self.enc=enc
        
        ## An integer that declares the delay time in microseconds between runs of task1
        self.interval1=interval1
        
        
        # The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        # print('Start time:'+str(self.start_time))
        
        # The "timestamp" for when task 1 should run next
        self.next_time1 = utime.ticks_add(self.start_time, self.interval1)
        # print('Next Time1:'+str(self.next_time1))
        
    def run(self):
        self.curr_time = utime.ticks_ms() #current time
        # print('Current time1:'+str(self.curr_time))
        if utime.ticks_diff(self.curr_time,self.next_time1)>0: #runs the task if current time is past the next time
            self.enc.update()
            self.next_time1 = utime.ticks_add(self.next_time1, self.interval1)
        