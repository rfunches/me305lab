'''
@file EncoderRunner.py

This file contains the timing component for data collection from the encoder. This is looped in the main method.
'''
import pyb
import utime
import shares


class EncoderRunner:
  
    
  
    def __init__(self,myenc,interval1):
        '''
        @brief            Creates an EncoderRunner object.
        @param myenc   An encoder object that is updated by this object
        @param interval1  An integer that declares the delay time in microseconds between runs 
        @param start_time  An integer timestamp for the first iteration
        @param next_time1  An integer timestamp for when the next run should be
        '''
  
        ## An encoder object that is to be run by this object
        self.myenc=myenc
        
        ## An integer that declares the delay time in microseconds between runs of task1
        self.interval1=interval1
        
        
        ## An integer timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        # print('Start time:'+str(self.start_time))
        
        # An integer "timestamp" for when the next run should be
        self.next_time1 = utime.ticks_add(self.start_time, self.interval1)
        # print('Next Time1:'+str(self.next_time1))
        
    def run(self):
        '''
        @brief   Runs the encoder's update based on the interval initialized in init
        '''
        self.curr_time = utime.ticks_ms() #current time
        #print('Current time1:'+str(self.curr_time))
        if utime.ticks_diff(self.curr_time,self.next_time1)>0: #runs the task if current time is past the next time
            self.myenc.update() #update encoder position
            self.next_time1 = utime.ticks_add(self.next_time1, self.interval1) #set the next time for update() to run
        
        
            
   

