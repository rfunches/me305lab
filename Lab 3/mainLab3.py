'''
@file main.py

This file contains the Main method for motor encoder Lab 3. The timer is initialized, the 
encoder, encoder runner, and user interface are declared, and the encoder and user interface
tasks are set to run on an infinite loop.
'''

import pyb
from Encoder import Encoder
from EncoderRunner import EncoderRunner
from userface import UserInterface


# pinA6=pyb.Pin.cpu.A6
# pinA7=pyb.Pin.cpu.A7
period=0xffff #Period of timer

tim=pyb.Timer(3) #Declare timer 3
tim.init(prescaler=0,period=0xFFFF) #Initialize timer 3 with prescale and period
tim.channel(1,pin=pyb.Pin.cpu.A6,mode=pyb.Timer.ENC_AB) #channel 1 of timer 3 to pin A6
tim.channel(2,pin=pyb.Pin.cpu.A7,mode=pyb.Timer.ENC_AB) #chennel 2 of timer 3 to pin A7


myenc=Encoder(tim,period) #declare encoder object, pass timer and period

interval1=100 #Interval at which FSM will run tasks in ms
EncControl=EncoderRunner(myenc, interval1) #Declare EncoderRunner to control timing of Encoder updates

interact=UserInterface(interval1) #Declare user interface that will interact with EncoderRunner through Shares.py

while(True): #infinite loop
    EncControl.run() #Run the task inside of EncoderRunner (Updates encoder according to timing)
    # print(str(EncControl.myenc.get_position()))
    interact.run() #Look for user input and act on it if exists
    
    

