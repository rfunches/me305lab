'''
@file BLEModuleDriver

This file contains the module that drives Bluetooth communication between
the microcontroller and a smartphone.
'''
#pyb works with the pins on the board
import pyb

#utime is used to work with millisecond counter on board
import utime

class BLEModuleDriver:
    '''
    @brief      This class interacts with hardware and is used to read to and from 
    UART, as well as control LED.
    '''
    
    
    def __init__(self,myuart):
        '''
        @brief            Creates a BLEModuleDriver Object
        @param myuart     UART Serial Communication Tool used to read from and write to a bluetooth device
        @param value      A Boolean representing the state of the LED. True means ON and False means OFF.
        '''
        
        #Initializes the pin that will be interacted with to use the LED on the board
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        self.tim2 = pyb.Timer(2, freq = 20000)
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
        
        ## UART Serial Communication Tool used to read from and write to a bluetooth device
        self.myuart=myuart #used for serial communication
        
        ##A Boolean representing the state of the LED. True means ON and False means OFF.
        self.value=False #LED Starts off
        
        
    def readFrom(self):
        '''
        @brief This function reads the current line in the UART and returns either 11,
        if the value from readline() is not valid, or the value that the user requests. If value
        is invalid, an error message is returned.
        '''
        val=self.myuart.readline()
        if(self.is_float(val)==False):
            self.write('Please enter a float on the next run that is between 0 and 10')
            return 11
        else:
            val=float(val)
            if(val<1 or val>10):
                self.write('Please enter a float between 0 and 10 on the next run')
                return 11
            else:
                return val
        
              
     
    def is_float(self,n):
       '''
       @brief  This function is used to ensure that a passed string is able to be converted
    to a float. The general outline of this method was obtained from the following link:
    https://note.nkmk.me/en/python-check-int-float/#:~:text=Check%20if%20a%20number%20is%20integer%20or%20decimal,3%20Check%20if%20numeric%20string%20is%20integer.%20'''
        try:
            float(n)
        except ValueError:
            return False #if the attempt to convert the value to float has an error, return false immediately
        else:
            n=float(n)
            return isinstance(n,float) #returns boolean determining if value is float
    
        
    def write(self,message):
        '''
        @brief  This function writes a string to the UART to be received by a Bluetooth connected device.
        '''
        self.myuart.write(message)
    
    def check(self):
       '''
       @brief  This function checks if anything is waiting in the UART to be read. If there is data to be read, True
       is returned. Else, False is returned.
       '''
        if self.myuart.any() !=0:
            return True
        else:
            return False
        
    def LEDset(self,value):
        '''
        @brief  This function sets the brightness of the LED based on a passed boolean.
        If the passed value is True, the LED is set to full brightness. If the passed value is False,
        the LED is turned off.
        '''
        if(value==True):
            self.t2ch1.pulse_width_percent(100)
        elif(value==False):
            self.t2ch1.pulse_width_percent(0)
        self.value=value
        
    def returnValue(self):
        '''
        @brief  This function returns the state of the LED. True is on and False is off.
        '''
        return self.value


