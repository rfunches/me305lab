'''
@file bluetoothFSM.py

This file contains the FSM that updates the LED's Status and deals with user input
at a fixed rate. These two states are transitioned between and the run() function is run
in an infinte loop in mainlab5.py
'''
import pyb
import utime

class bluetoothFSM:
    
    '''
    @param S0_init  State 0, Initialization
    @param S1_Blink_LED  State 1, where the LED is turned to the opposite of its current value if the timing requirement is met
    @param S2_Check_For_Input  State 2, where the task checks to see if the user has commanded the machine to change frequency. Frequency is then updated.
    '''
    ##State 0, Initialization
    S0_init=0
    
    ##State 1, where the LED is turned to the opposite of its current value if the timing requirement is met
    S1_Blink_LED=1
    
    ##State 2, where the task checks to see if the user has commanded the machine to change frequency. Frequency is then updated.
    S2_Check_For_Input=2
  
    def __init__(self,myuart,interval,BLEDrive):
        '''
        @brief            Creates an EncoderRunner object.
        @param interval  An integer that declares the period for the square wave of the LED's brightness
        @param start_time  An integer timestamp for the first iteration
        @param next_time1  An integer timestamp for when the next run should be. Only half of the interval is added because one period of the LED square wave consists of the LED turning on, off, and then on again, which means that the LED must update twice to go through one full cycle.
        @param myuart     Serial communication object used for communication between Nucleo and smartphone.
        @param BLEDrive   Bluetooth Driver object used to interact with hardware
        @param state   An integer that keeps track of the current state of this object in the FSM
        '''
        ##An integer that declares the period for the square wave of the LED's brightness
        self.interval=interval
        
        ## The integer timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        # print('Start time:'+str(self.start_time))
        
        ##erial communication object used for communication between Nucleo and smartphone.
        self.myuart=myuart #Passed UART for communication with the computer from main.py
        
        ##Bluetooth Driver object used to interact with hardware
        self.BLEDrive=BLEDrive
        
        # The "timestamp" for when the next run should be
        self.next_time1 = utime.ticks_add(self.start_time, int(self.interval/2))
       
        ## An integer that keeps track of the current state of this object in the FSM
        self.state=self.S0_init #Initial state of the FSM
        
      
       
        
    def run(self):
        '''
        @brief   Runs the encoder's FSM based on the current interval.
        '''
        
        if(self.state == self.S0_init): #State 0
            self.transitionTo(self.S1_Blink_LED) #transition to state 1
            
             
        elif(self.state == self.S1_Blink_LED): #State 1
            self.curr_time = utime.ticks_ms() #current time
            if utime.ticks_diff(self.curr_time,self.next_time1)>0: #runs the task if current time is past the next time
                self.BLEDrive.LEDset(not(self.BLEDrive.returnValue())) #set the LED to the opposite of the current value
                self.next_time1 = utime.ticks_add(self.next_time1, int(self.interval/2)) #set the next time for the task to actually run
                #interval/2 used because this function is used to turn the LED on and off
                #in order for the full period of the square wave to be within the interval
                # timeframe, the function must run twice as often as the period
            
            self.transitionTo(self.S2_Check_For_Input) #transition to state 2
            
            
        elif(self.state == self.S2_Check_For_Input): #state 2
            if(self.BLEDrive.check()==True): #if there is a value waiting in the UART
                newfreq=self.BLEDrive.readFrom() #read from the UART
                if(newfreq>=1 and newfreq<=10): #readFrom() returns 11 if value is nonsensical. Otherwise, makes sure input is between 1 and 10hz.
                    self.updateFrequency(newfreq) #update frequency
                    
            
            self.transitionTo(self.S1_Blink_LED) #transition to state 1
                    
                    
    
                   
    def updateFrequency(self,newfreq):
        '''
        @brief Updates frequency by taking user input and setting the interval to 1/freq
        and converted to microseconds. A message is also displayed to tell reader what the new frequency is.
        '''
        self.interval=(1/newfreq)*1000 #period=1/freq
        self.BLEDrive.write('LED Blinking at '+str(newfreq)+' Hz.')
        
        
    def transitionTo(self,newstate): 
        '''
        @brief This function transitions the state variable to reflect the current state inside the FSM.
        
        '''
        self.state=newstate 
