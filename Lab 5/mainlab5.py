'''
@file mainlab5.py
The main method of Lab 5 is used to initialize all values and run the Finite State Machine(FSM)
in an endless loop. After initializing the UART for serial communication with a bluetooth device, based on Table 17 of the Nucelo's Datasheet,
the Pin Connected to the Bluetooth Tranceiver's Enable Pin is set low to allow for outside connection. This module also declares BLEModuleDriver and 
bluetoothFSM objects and runs the finite state machine in a loop. 

'''

import pyb
from pyb import UART
import utime

from BLEModuleDriver import BLEModuleDriver
from bluetoothFSM import bluetoothFSM


myuart=pyb.UART(3,9600) #initialize UART for communication with phone

pinC2=pyb.Pin(pyb.Pin.cpu.C2,pyb.Pin.OUT_PP) #initialize pin that works with Bluetooth Enable pin as an output
pinC2.low() #pin set low

BLEDrive=BLEModuleDriver(myuart) #BLEModuleDriver object declared
initinterval=500 #initial period for LED in seconds
MyFSM=bluetoothFSM(myuart,initinterval,BLEDrive)  #bluetoothFSM object declared

while True: #run FSM
    MyFSM.run() 
