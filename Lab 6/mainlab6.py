'''
@file mainlab6.py

This file contains the main method that is used to initialize all values and then 
initialize two different motor driver objects for the two physical motors on the board.
'''
import pyb
import utime
from MotorDriver import MotorDriver 

pinA15=pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP) #set nSleep pin to be an output pin
pinB4=pyb.Pin(pyb.Pin.cpu.B4) #initialize pin relating to IN1 for motor 1
tim3=pyb.Timer(3,freq=20000) #initialize timer 3 that works with all pins
pinB5=pyb.Pin(pyb.Pin.cpu.B5) #initialize pin relating to IN2 for motor 1


pinB0=pyb.Pin(pyb.Pin.cpu.B0) #initialize pin relating to IN3 for Motor 2
pinB1=pyb.Pin(pyb.Pin.cpu.B1) #initialize pin relating to IN4 for Motor 2

#initialize motor objects by passing nSLEEP pin, two enable pins, and motor number to initialize channel
moe1=MotorDriver(pinA15,pinB4,pinB5,tim3,1)
moe2=MotorDriver(pinA15,pinB0,pinB1,tim3,2)

#enable motors
moe1.enable() 
moe2.enable()
