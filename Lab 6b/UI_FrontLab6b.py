'''
@file UI_FrontLab6.py

This file contains the UI Front end to be run on the computer for data collection with
an encoder. After setting up serial communication capabilities, an input for Kp and omegaref are requested from the user. As 
we are testing the controller response to a step input, the omegaref input will be a constant value. After sufficient time for the motor to 
reach steady state operation, allotted as 5 seconds, data return is done through the serial port. The time and velocity lists are sent to the computer one line at a time
and stored in local lists. Finally, the data is plotted for user visualization.

'''
import array
import time
import serial
import numpy
import matplotlib


ser = serial.Serial(port='COM3',baudrate=115273,timeout=1) #initializes serial port for communication with Nucelo %changed from COM3 to COM5


print('Welcome to the UI for Control System Interaction') #welcomes to frontend
Kp=input('Give me a value for Kp in V/rpm  ') #asks for user input for Kp
Kp=float(Kp) #casts Kp as a float
ser.write('{:2.2f}'.format(Kp).encode('ascii')) #writes Kp to controller through serial port
omegaref=input('Give me a value for omegaref in rpm ') #asks for user input for omegaref
omegaref=float(omegaref) #casts omegaref as a float
ser.write('{:4.1f}\r\n'.format(omegaref).encode('ascii')) #writes omegaref to controller through serial port %removed \r\n

        
t=[] #initialize time list
velocity=[]  #initialize velocity list
time.sleep(5) #stall for 5 seconds to ensure that all variables put into UART on Nucleo side


vlength=ser.readline().decode('ascii') #obtain length of velocity list
vlength=int(vlength) #velocity list length cast as int, used for loop control

for n in range(1,vlength): #loop through entire list
    line_string = ser.readline().decode('ascii') #read the line which has time and velocity comma seperated
    line_string=(line_string.strip()) #strips all special characters from String, such as \r and \n
    print(line_string)								# print the line to see in command prompt
    line_list = line_string.split(',')		#split string at comma, 0th entry is time, 1st entry is velocity
    t.append(float(line_list[0]))				#append 0th entry to t list	
    velocity.append(float(line_list[1]))		#append 1st entry to velocity list


#plotting
matplotlib.pyplot.plot(t,velocity)	#plot command
matplotlib.pyplot.xlabel('Time (ms)') #x-axis label
matplotlib.pyplot.ylabel('Velocity (RPM)') #y-axis label
matplotlib.pyplot.title('Velocity vs Time, Omegaref={:4.0f}rpm, Kp={:} V/rpm'.format(omegaref,Kp))

#save as CSV file
numpy.savetxt('dataoutput.csv',list(zip(t,velocity)),delimiter=',') #output csv file from 2D list


