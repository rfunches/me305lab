'''
@file closedloop.py

This file implements a closed loop P control model to designate the voltage to apply to
the motor given the current speed. 
'''

import pyb
import utime

class closedloop:
    '''
    @brief  This Class implements a closed loop control algorithm'''
    
    ##Voltage applied to motor when level=100
    Vdc=3.3 
    
    def __init__(self,Kp,omegaref):
        '''
        @brief  Makes closed loop object
        @param Kp  Float that designates the position controller gain for the control system (V/rpm)
        @param omegaref  Float that designates reference velocity of motor (rpm)
        '''        
        
        ##Float that designates the position controller gain for the control system (V/rpm)
        self.Kp=Kp
        
        ##Float that designates reference velocity of motor (rpm)
        self.omegaref=omegaref #rpm
        
        
    def run(self,omega):
        '''
        @brief  Based on current speed, obtain new level for motor to get closer to omegaref
        '''
        level=(self.Kp)/(self.Vdc)*(self.omegaref-omega) #scale error by Kp, divide by max voltage to get percentage of maximum voltage
        #saturation
        if(level>100): #if level greater than 100
            return 100 #saturation
        elif(level<-100): #if level less than -100
            return -100 #saturation
        else:
            return level
        
    def get_Kp(self):
        '''
        @brief This function acts as a getter for Kp.
        '''
        return(self.Kp)
        
        
    def set_Kp(self,Kp):
        '''
        @brief  This function acts as a setter for Kp.
        '''
        self.Kp=Kp
            
    
        
                
        