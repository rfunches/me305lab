'''
@file task_controller.py

This file represents a controller object that interacts with hardware to ensure motor speed control.
The controller object's run() function acts as a single loop through a closed loop block diagram. After current output speed is 
given by the encoder, the closed loop object acts on the speed to determine the level to set the motor based on a closed loop P control model.
Finally, this object sets the level of the motor accordingly.
'''

import pyb
import utime

class controller:
    '''
    @brief  This Class implements a control system, which interacts with hardware and the designated 
    control model.'''
    
     
    
    def __init__(self,Kp,omegaref,mot,enc,cloop):
        '''
        @brief  Makes controller object
        @param Kp  Float that designates the position controller gain for the control system (V/rpm)
        @param omegaref  Float that designates reference velocity of motor (rpm)
        @param mot   Motor object that rotates output shaft proportional to the duty cycle of the motor
        @param enc  Encoder object that is used to track the position of the output shaft
        @param cloop  Closed loop object that is used to determine the voltage to apply to the motor given the current speed of the motor        
        '''
        ##Float that designates the position controller gain for the control system (V/rpm)
        self.Kp=Kp #passed Kp (V/rpm)
        
        ##Float that designates reference velocity of motor (rpm)
        self.omegaref=omegaref #designated reference velocity in rpm
        
        ##Motor object that rotates output shaft proportional to the duty cycle of the motor
        self.mot=mot
        
        ##Encoder object that is used to track the position of the output shaft
        self.enc=enc
        
        ##Closed loop object that is used to determine the voltage to apply to the motor given the current speed of the motor       
        self.cloop=cloop
        
        
    def run(self):
        '''
        @brief  Run controller, return current value for speed
        '''
        self.enc.update() #updates encoder to find current delta 
        omega=self.enc.get_speed() #obtain current velocity based on delta and interval time
        level=self.cloop.run(omega) #obtain level to apply to motor by passing current speed to cloop
        self.mot.set_duty(level) #set duty cycle on motor input voltage based on output from cloop
        return omega #return omega to task_user to append onto velocity list
        
    def setOmegaRef(self,omegaref):
        '''
        @brief This function is a setter for omegaref, which changes it in both this class and the closedloop class.
        '''
        self.omegaref=omegaref
        self.cloop.omegaref=omegaref
        
    def getOmegaRef(self):
        '''
        @brief This function returns the current value of omegaref held in this class.
        '''
        return self.omegaref
        
    
    
        
    
    
    
                
        