'''
@file task_user.py

This file is used to communicate with the computer through serial communication, initialize
all objects, and loop the controller run() function for an appropriate time interval. This file takes
input reference velocity and Kp from the computer and returns a list for velocity and time.
'''
import pyb
from pyb import UART
import utime
from task_controller import controller
from closedloop import closedloop
from EncoderLab6 import Encoder
from MotorDriver import MotorDriver

#Initialize serial communication

myuart=pyb.UART(2) #initialize UART for communication with computer

#waiting for Kp
n=0 #loop control variable
while(n==0): #runs while n==0, changed from 0 when Kp received
    if myuart.any() != 0: #if there's anything from the computer
        Kp = myuart.read().decode('ascii') #read the line
        Kp=float(Kp)
        n=1

#waiting for omegaref
n=0 #loop control variable
while(n==0): #runs while n==0, changed from 0 when omegaref received
    if myuart.any() != 0: #if there's anything from the computer
        omegaref = myuart.read().decode('ascii') #read the line
        omegaref=float(omegaref)
        n=1


# Kp=0.05
# omegaref=3000 #reference velocity in rpm

#Encoder Stuff
period=0xffff #Period of timer

tim=pyb.Timer(4) #Declare timer 4
tim.init(prescaler=0,period=0xFFFF) #Initialize timer 4 with prescale and period
tim.channel(1,pin=pyb.Pin.cpu.B6,mode=pyb.Timer.ENC_AB) #channel 1 of timer 4 to pin B6
tim.channel(2,pin=pyb.Pin.cpu.B7,mode=pyb.Timer.ENC_AB) #chennel 2 of timer 4 to pin B7

interval=30 #Interval at which FSM will run tasks in ms
CPR=1000 #CPR of motor
enc=Encoder(tim,period,interval,CPR) #declare encoder object, pass timer,period, interval




#Motor stuff
pinA15=pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP) #set nSleep pin to be an output pin
pinB4=pyb.Pin(pyb.Pin.cpu.B4) #initialize pin relating to IN1 for motor 1
tim3=pyb.Timer(3,freq=20000) #initialize timer 3 that works with all pins
pinB5=pyb.Pin(pyb.Pin.cpu.B5) #initialize pin relating to IN2 for motor 1

mot=MotorDriver(pinA15,pinB4,pinB5,tim3,1)#initialize motor objects by passing nSLEEP pin, two enable pins, and motor number to initialize channel
mot.enable() #enable motor


#closed loop control/controller

cloop=closedloop(Kp,omegaref) #closed loop object initialized, pass Kp, omegaref

controller=controller(Kp,omegaref,mot,enc,cloop) #controller object initialized, pass Kp, omegaref, interval, mot,enc,cloop


#Data collection
TimeData=[] #list to store time values in ms
SpeedData=[] #list to store velocity data in rpm


#timing
start_time = utime.ticks_ms() #time for data collection to start

next_time = utime.ticks_add(start_time, interval) #next time for control loop to run defined as start_time+interval

while((next_time-start_time)<1000): #motor reaching steady state takes under 1 second, so run control system for 1000ms
    curr_time=utime.ticks_ms() #current time 
    if utime.ticks_diff(curr_time,next_time)>0: #if current time is past the designated next time
        speed=controller.run() #current speed calculated by encoder inside controller, also causes a run of controller which drives hardware
        time=curr_time-start_time #current change in time relative to start time
        TimeData.append(time) #append time to time list
        SpeedData.append(speed) #append velocity to velocity list
        
        next_time = utime.ticks_add(next_time, interval) #set next time for controller to run
    


myuart.write('{:}\r\n'.format(len(SpeedData))) #passes length of list back to main in order to control loop
for n in range(len(SpeedData)): #loops through entirety of time and velocity lists
        myuart.write('{:},{:}\r\n'.format(TimeData[n],SpeedData[n])) #write values of time and velocity seperated by a comma at current index
                                      
mot.disable() #disable motor after data returned to ensure safety
