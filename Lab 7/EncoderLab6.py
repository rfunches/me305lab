'''
@file EncoderLab6.py

This file contains the encoder class that is used in conjunction with EncoderDriver.py
in order to collect data from the encoder. After initialization, the encoder is able to
update, which sets position based on the delta that can be calculated from the timer.
Based on the design of the timer, underflow or overflow can occur, which requires correction. Correction
is needed if the magnitude of delta is greater than or equal to half of the period.
'''

import pyb
import shares


class Encoder:
    '''
    @brief      This class interacts with encoder hardware and updates encoder position on the MCU
    @param position   Integer position of encoder from counter, updated by the update() method
    @param zeroer   Integer that is subtracted from delta in order to zero the position of the encoder after user input, updated at end of update()   
       
    '''
    ##Integer position of encoder from counter, updated by the update() method
    position=0
    
    ##Integer that is subtracted from delta in order to zero the position of the encoder after user input, updated at end of update() 
    zeroer=0
    
    def __init__(self,tim,period,interval,CPR):
        
        
        '''
         brief      Creates an encoder object
         @param tim    Timer object used with two channels connected to encoder phase
         @param period   Period for timer
        '''
        
        ##Timer object used with two channels connected to encoder phase
        self.tim=tim
        
        ##Period for timer
        self.period=period
        
        self.interval=interval
        
        self.CPR=CPR
  
           
           
    def update(self):
        '''
         @brief      Updates encoder position
         '''
                    
        curread=self.tim.counter() #obtain current reading on encoder
        
        #Compute delta from new reading and current position, zeroer is additionally subtracted
        delta=self.get_delta(self.position+self.zeroer,curread) 
        
        if(abs(delta)>=(self.period/2)): #bad delta if greater than or equal to half of the period
            if(delta>0): #positive bad delta
                delta=delta-self.period #correct by subtracting period
            else: #negative bad delta
                delta=delta+self.period #correct by adding period
        
        self.delta=delta
        # Look at shares.py, which contains shared variables between user interface and this class.
        # If clearpos==1, user desires the position reading to be zeroed.
        if(shares.clearpos==1): 
            self.zeroer+=self.get_position() #Current position is added to zeroer, this is subtracted from delta to decrease/increase future deltas
            self.set_position(0) #The current position is set to 0
            shares.clearpos=0 #The reset variable is reset
         
        newpos=self.position+delta #new position as calculated as the old position plus the corrected delta
        self.set_position(newpos) #new position is set
        
        #The current delta and position readings are put into shares.py for interaction with the user interface.
        shares.delta=delta
        shares.position=newpos
        
    def get_position(self):
        '''
         @brief      Returns encoder position in pulses
         '''
        return self.position
    
    def set_position(self,newpos):
        '''
         @brief      Sets encoder position in ticks
         '''
        self.position=newpos
        
    def get_positionDeg(self):
        '''
        @brief   Returns encoder position in degrees
        '''
        conv=self.CPR*4/360 #yields ticks/deg
        posdeg=self.position/conv
        return posdeg
        
    def get_delta(self,position,curread):
        '''
         @brief      Returns the difference between two values that are passed to this method
         '''
        delta=curread-position
        self.delta=delta
        return delta
    
    def get_speed(self):
        '''
        @brief Return speed in rpm
        '''
        conv=self.CPR*4 #yields ticks/rev
        dx=(self.delta)/conv #revolutions since last check
        dt=self.interval/(60000) #interval converted from ms to minute
        speed=dx/dt #calculate speed from change in position over change in time
        return speed
    
    
           