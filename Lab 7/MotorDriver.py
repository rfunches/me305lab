''' 
@file MotorDriver.py
This class has been designed to work with physical motor hardware. Pulse Width Modulation (PWM) is used
in order to drive motor based on a designated user input. The motor is also able to be enabled and disabled
by setting the nSLEEP pin high or low respectively.
'''

import pyb
import utime

class MotorDriver:
    '''
    @brief  This Class implements a motor'''
    
    def __init__(self,nSLEEP_Pin,IN1_Pin,IN2_Pin,timer,motnum):
        '''
        @brief  Makes motor object
        @param nSLEEP_Pin Enable pin
        @param IN1_Pin  In 1 Pin
        @param IN2_Pin  In 2 Pin
        @param timer  Timer that works with PWM for IN1_Pin and IN2_Pin
        '''
        # print('Creating a motor driver')
        ##Enable Pin
        self.nSLEEP_Pin=nSLEEP_Pin
        ##In 1 Pin
        self.IN1_Pin=IN1_Pin
        ##In 2 Pin
        self.IN2_Pin=IN2_Pin
        ##Timer that works with PWM for IN1_Pin and IN2_Pin
        self.timer=timer
        if(motnum==1): #if Motor number 1, set the channels on timer for PB4 and PB5
            self.tch1=self.timer.channel(1,pyb.Timer.PWM,pin=self.IN1_Pin)
            self.tch2=self.timer.channel(2,pyb.Timer.PWM,pin=self.IN2_Pin)
        elif(motnum==2): #if Motor number 2, set the channels on timer for PB0 and PB1
            self.tch1=self.timer.channel(3,pyb.Timer.PWM,pin=self.IN1_Pin)
            self.tch2=self.timer.channel(4,pyb.Timer.PWM,pin=self.IN2_Pin)
        # print('Motor Driver Created')
        
    def enable(self):
        '''
        @brief This function enables the motor to run by setting nSLEEP high.
        '''
        self.nSLEEP_Pin.high()
        # print('Enabling motor')
        
        
    def disable(self):
        '''
        @brief This function disables the motor by setting nSLEEP low.
        '''
        self.nSLEEP_Pin.low()
        # print('Disabling motor')
        
        
    def set_duty(self,duty):
        '''
        @brief This function sets the duty cycle on the PWM for the motor, which by 
        increasing average voltage applied to the motor, will increase motor speed. This function checks
        to make sure that the input is valid, and then sets the pins to the proper value to ensure the right 
        duty cycle in the correct direction.
        '''
        if(not(self.is_float(duty))): #check to make sure that the passed value is a float
            print('Please enter a float between -100 and 100')
        else:
            if(duty>=0): #rotate forward
                self.tch1.pulse_width_percent(duty) #set forward pin to proper duty cycle
                self.tch2.pulse_width_percent(0) #keep other pin low
            else: #reverse direction
                self.tch1.pulse_width_percent(0) #set forward pin low
                self.tch2.pulse_width_percent(-1*duty) #set reverse pin to magnitude of passed value. Multiplying by negative corrects sign.
            
            ##Duty cycle which controls average voltage applied to motor, controlling motor output speed
            self.duty=duty
            # print('Running with new duty cycle of '+str(duty)+' %')
    
    
    def is_float(self,n):
        '''
       @brief  This function is used to ensure that a passed string is able to be converted
       to a float. The general outline of this method was obtained from the following link:
           https://note.nkmk.me/en/python-check-int-float/#:~:text=Check%20if%20a%20number%20is%20integer%20or%20decimal,3%20Check%20if%20numeric%20string%20is%20integer.%20'''
        try:
            float(n)
        except ValueError:
            return False #if the attempt to convert the value to float has an error, return false immediately
        else:
            n=float(n)
            return isinstance(n,float) #returns boolean determining if value is float
    

# if __name__=='__main__':
#     pinA15=pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)
#     pinB4=pyb.Pin(pyb.Pin.cpu.B4)
#     tim3=pyb.Timer(3,freq=20000)
#     t3ch1=tim3.channel(1,pyb.Timer.PWM,pin=pinB4)
#     pinB5=pyb.Pin(pyb.Pin.cpu.B5)
#     t3ch2=tim3.channel(2,pyb.Timer.PWM,pin=pinB5)
    
#     moe1=MotorDriver(pinA15,pinB4,pinB5,t3ch1,t3dch2)
#     moe1.enable()
    
    
#>>> t3ch1.pulse_width_percent(30)
#>>> t3ch2.pulse_width_percent(0)
#>>> pinA15.high()

