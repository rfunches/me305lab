'''
@file UI_FrontLab7.py

This file contains the UI Front end to be run on the computer for data collection with
an encoder. After setting up serial communication capabilities, an input for Kp is requested from the user
and the interval at which data is taken is defined on the front end. The reference values that we are comparing our motor performance to are read from a CSV file while waiting for data return. 
After sufficient time for the motor to respond to the 15 second velocity profile, data return is done through the serial port. The time, position and velocity lists are sent to the computer one line at a time
and stored in local lists. The value J is computed as a performance parameter representing error in position and velocity between the reference values and the experimental values caused by the control system. Finally, the data is plotted for user visualization.

'''
import array
import time
import serial
import numpy
import matplotlib


ser = serial.Serial(port='COM3',baudrate=115273,timeout=1) #initializes serial port for communication with Nucelo %changed from COM3 to COM5


#ask for user input, write Kp and interval to UART
print('Welcome to the UI for Control System Interaction') #welcomes to frontend
Kp=input('Give me a value for Kp in V/rpm  ') #asks for user input for Kp
Kp=float(Kp) #casts Kp as a float
ser.write('{:2.2f}\r\n'.format(Kp).encode('ascii')) #writes Kp to controller through serial port

time.sleep(2) #wait 2 seconds before sending interval, can't send too fast to backend

interval=35 #interval between runs of the feedback loop in ms. This value had to be increased since lab 6 due to low memory on the backend.
ser.write('{:}\r\n'.format(interval).encode('ascii')) #send interval to Nucleo


#Import from CSV, used from Charlie's Piazza post showing a method on how to do this
# empty lists for data
tref = [] #time column from CSV (ms)
omegaref = [] #velocity column from CSV (rpm)
posref = []#position column from CSV (deg)

# Open the file. 
ref = open('reference.csv');

# "Read data indefinitely. Loop will break out when the file is done."-Charlie
scaler=20 #need to scale position and time ref by factor of 20 so that my motor can run the shape of distribution
n=0 #used to ensure that the only values that are read from the CSV are the values when the control system will run (multiples of the interval)
while True:
    # Read a line of data. It should be in the format 't,v,x\n' but when the
    # file runs out of lines the lines will return as empty strings, i.e. ''-Charlie
    line = ref.readline()
    
    # If the line is empty, there are no more rows so exit the loop
    if line == '':
        break
    
    # If the line is not empty, strip special characters, split on commas, and
    # then append each value to its list.
    elif(n%interval==0):#used to ensure that the only values that are read from the CSV are the values when the control system will run (multiples of the interval)
        (t,v,x) = line.strip().split(','); #obtain t,v,x from a single line of CSV, remove special characters, split on ','
        t=float(t) #cast t as float
        t=t*1000 #multiply by 1000 to get ms
        tref.append(int(t)) #append integer time to list
        v=float(v) #cast v as a float
        v=v*scaler #multiply v by scaler to obtain real reference profile
        omegaref.append(v) #append modified v to reference velocity list
        x=float(x) #cast x as a float
        x=x*scaler #multiply v by scaler to obtain real reference profile
        posref.append(x)#append modified x to reference position list
    n=n+1 #increment n
ref.close() #close communication with CSV



#Real Values
t=[] #initialize time list
velocity=[]  #initialize velocity list
position=[] #initialize position list
time.sleep(35) #stall for 35 seconds to ensure that all variables put into UART on Nucleo side, time for script to run on backend


vlength=ser.readline().decode('ascii') #obtain length of velocity list
vlength=int(vlength) #velocity list length cast as int, used for loop control

for n in range(1,vlength): #loop through entire list
    line_string = ser.readline().decode('ascii') #read the line which has time and velocity comma seperated
    line_string=(line_string.strip()) #strips all special characters from String, such as \r and \n
    print(line_string)								# print the line to see in command prompt
    line_list = line_string.split(',')		#split string at comma, 0th entry is time, 1st entry is velocity
    t.append(float(line_list[0]))				#append 0th entry to t list	
    velocity.append(float(line_list[1]))		#append 1st entry to velocity list
    position.append(float(line_list[2]))       #append 2nd entry to position list
    


#Computing performance parameter J
J=0 #start sum with J=0

for k in range(0,vlength-1): #loop through the entirety of data lists
    J=J+(omegaref[k]-velocity[k])*(omegaref[k]-velocity[k])+(posref[k]-position[k])*(posref[k]-position[k]) #sum up square of error terms as according to equation given in Lab manual
J=J/vlength #divide the total J by number of terms 
J=int(J) #cast as integer for simplicity in viewing

#plotting
matplotlib.pyplot.subplot(2,1,1) #top subplot
matplotlib.pyplot.plot(t,velocity,label='Measured')	#plot experimental
matplotlib.pyplot.plot(tref,omegaref,label='Reference')#plot reference
matplotlib.pyplot.xlabel('Time (ms)') #x-axis label
matplotlib.pyplot.ylabel('Velocity (RPM)') #y-axis label
matplotlib.pyplot.title('Motor Position and Velocity vs Time,  Kp={:} V/rpm, J={:}'.format(Kp,J)) #plot title
matplotlib.pyplot.legend() #add legend to show experimental and reference values

matplotlib.pyplot.subplot(2,1,2) #bottom subplot
matplotlib.pyplot.plot(t,position,label='Measured Position')	#plot experimental
matplotlib.pyplot.plot(tref,posref,label='Reference Position')  #plot reference
matplotlib.pyplot.xlabel('Time (ms)') #x-axis label
matplotlib.pyplot.ylabel('Position (Deg)') #y-axis label


#save as CSV file
numpy.savetxt('dataoutput.csv',list(zip(t,velocity)),delimiter=',') #output csv file from 2D list














#Extra
# treflength=len(tref)
# #write csv values to UART
# ser.write('{:}\r\n'.format(treflength).encode('ascii')) #passes length of list back to main in order to control loop     
# for n in range(len(tref)):#loops through entirety of time and velocity lists
#     ser.write('{:},{:}\r\n'.format(tref[n],omegaref[n]).encode('ascii'))#write values of time and velocity seperated by a comma at current index
#     print('{:},{:}\r\n'.format(tref[n],omegaref[n]))


