'''
@file task_userLab7.py

This file is used to communicate with the computer through serial communication, initialize
all objects, and loop the controller run() function for an appropriate time interval. This file takes
input Kp from the computer and interacts with the CSV velocity reference values that are uploaded onto the Nucleo. As there is not enough room
to scale values in the CSV such that I can run this profile at a higher speed, it is done while reading from file. This file interacts with the controller, which controls and drives hardware,
and returns a list for experimental velocity, position, and time.
'''
import pyb
from pyb import UART
import utime
import array
from task_controller import controller
from closedloop import closedloop
from EncoderLab6 import Encoder
from MotorDriver import MotorDriver

#Initialize serial communication

myuart=pyb.UART(2) #initialize UART for communication with computer

#waiting for Kp
n=0 #loop control variable
while(n==0): #runs while n==0, changed from 0 when Kp received
    if myuart.any() != 0: #if there's anything from the computer
        Kp = myuart.read().decode('ascii') #read the line
        Kp=float(Kp) #cast Kp as float
        n=1 #break out of loop
        
        
#interval passed over from front end
n=0 #loop control variable
while(n==0): #runs while n==0, changed from 0 when Kp received
    if myuart.any() != 0: #if there's anything from the computer
        interval = myuart.read().decode('ascii') #read the line,Interval at which FSM will run tasks in ms
        interval=float(interval) #cast interval as float
        n=1 #break out of loop


#Import from CSV, used from Charlie's Piazza post showing a method on how to do this
# Blank lists for data. I am not importing position because I am only using
#velocity to control. Comparison between reference and experimental position is 
#done on the front end
# Use of array rather than list saves memory on backend
tref = array.array('i',[]) #time column from CSV (ms)
omegaref = array.array('f',[]) #velocity column from CSV (rpm)

# Open the file. 
ref = open('reference.csv');

# Read data indefinitely. Loop will break out when the file is done.

n=0#used to ensure that the only values that are read from the CSV are the values when the control system will run (multiples of the interval)
while True:
    # Read a line of data. It should be in the format 't,v,x\n' but when the
    # file runs out of lines the lines will return as empty strings, i.e. ''-Charlie
    line = ref.readline()
    
    # If the line is empty, there are no more rows so exit the loop
    if line == '':
        break
    
    # If the line is not empty, strip special characters, split on commas, and
    # then append each value to its list.
    elif(n%interval==0):#used to ensure that the only values that are read from the CSV are the values when the control system will run (multiples of the interval)
        (t,v) = line.strip().split(','); #obtain t,v from a single line of CSV, remove special characters, split on ','
        t=float(t) #cast t as float
        t=t*1000 #convert to ms
        tref.append(int(t)) #append time to integer array
        omegaref.append(float(v)) #append velocity to velocity array
    n=n+1 #increments n
ref.close() #close CSV communication



#Encoder Stuff
period=0xffff #Period of timer

tim=pyb.Timer(4) #Declare timer 4
tim.init(prescaler=0,period=0xFFFF) #Initialize timer 4 with prescale and period
tim.channel(1,pin=pyb.Pin.cpu.B6,mode=pyb.Timer.ENC_AB) #channel 1 of timer 4 to pin B6
tim.channel(2,pin=pyb.Pin.cpu.B7,mode=pyb.Timer.ENC_AB) #chennel 2 of timer 4 to pin B7

CPR=1000 #CPR of motor
enc=Encoder(tim,period,interval,CPR) #declare encoder object, pass timer,period, interval




#Motor stuff
pinA15=pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP) #set nSleep pin to be an output pin
pinB4=pyb.Pin(pyb.Pin.cpu.B4) #initialize pin relating to IN1 for motor 1
tim3=pyb.Timer(3,freq=20000) #initialize timer 3 that works with all pins
pinB5=pyb.Pin(pyb.Pin.cpu.B5) #initialize pin relating to IN2 for motor 1

mot=MotorDriver(pinA15,pinB4,pinB5,tim3,1)#initialize motor objects by passing nSLEEP pin, two enable pins, and motor number to initialize channel
mot.enable() #enable motor


#closed loop control/controller

cloop=closedloop(Kp,omegaref[0]) #closed loop object initialized, pass Kp, omegaref
controller=controller(Kp,omegaref[0],mot,enc,cloop) #controller object initialized, pass Kp, omegaref, interval, mot,enc,cloop


#Data collection, arrays used due to memory efficiency
TimeData=array.array('i',[]) #array to store time values in ms
SpeedData=array.array('f',[])#array to store velocity data in rpm
PosData=array.array('f',[])#array to store position data in rpm

#Timing (Different than Lab 6b)
start_time = utime.ticks_ms() #time for data collection to start
refcontrol=0 #used for loop control to loop through time array

while(refcontrol<len(tref)): #runs through the time matrix, goes to the next index as soon as the current time is past time at current index. At end of time array, loop breaks
    curr_time=utime.ticks_ms() #current time 
    if curr_time-start_time>=(tref[refcontrol]): #if current time is past the designated next time
        controller.setOmegaRef(omegaref[refcontrol]*20) #reference values scaled up by 20 so that my motor can run(changed based on frontend), hire operating speed required
        speed=controller.run() #current speed calculated by encoder inside controller, also causes a run of controller which drives hardware
        newpos=enc.get_positionDeg() #current position retreived from encoder
        PosData.append(newpos) #append position measurment to array
        time=curr_time-start_time #current change in time relative to start time
        TimeData.append(time) #append time to time list
        SpeedData.append(speed) #append velocity to velocity list
        
        refcontrol=refcontrol+1 #increment refcontrol if the control system ran
        
mot.disable() #disable motor after data returned to ensure safety        
    


myuart.write('{:}\r\n'.format(len(SpeedData))) #passes length of list back to main in order to control loop
for n in range(len(SpeedData)): #loops through entirety of time,position, velocity lists
        myuart.write('{:},{:},{:}\r\n'.format(TimeData[n],SpeedData[n],PosData[n])) #write values of time,velocity, and position seperated by commas at current index
                                      
        
        
        
        
        
        
        
        
        
        
        
        
      
        
      
        
      
        
      
#extra
# tref=[]
# omegaref=[]
# utime.sleep(5) #pause for 3 seconds before reading from UART based on passed CSV

# vlength=myuart.readline().decode('ascii') #obtain length of velocity list
# vlength=vlength.strip()
# vlength=int(vlength) #velocity list length cast as int, used for loop control

# t2ch1.pulse_width_percent(100)
# myuart.write('{:}'.format(vlength).encode('ascii')) #write values of time and velocity seperated by a comma at current index
# for n in range(1,vlength): #loop through entire list
#     t2ch1.pulse_width_percent(0)
#     line_string = myuart.readline().decode('ascii') #read the line which has time and velocity comma seperated
#     line_string=(line_string.strip()) #strips all special characters from String, such as \r and \n
#     t2ch1.pulse_width_percent(100) 
#     line_list = line_string.split(',')		#split string at comma, 0th entry is time, 1st entry is velocity
#     t2ch1.pulse_width_percent(0) 
#     tref.append(float(line_list[0]))				#append 0th entry to t list	
#     omegaref.append(float(line_list[1]))		#append 1st entry to velocity list
#     t2ch1.pulse_width_percent(100)
    
   



