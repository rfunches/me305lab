'''
@file EncoderRunner.py

This file contains the FSM which updates the encoder at a fixed rate and communicates
with the computer regarding both time of data collection and the data that is collected. The blinking of 
an LED was used to signify that data is currently being collected.

'''
import pyb
import utime
import shares


class EncoderRunner:
    '''
    @param S0_init  State 0 Initialization
    @param S1_Take_Data State 1, where encoder is updated and data is stored in a list
    @param S2_Stop_Check  State 2, where the task checks to see if the user has commanded the machine to stop collecting data
    @param S3_Return_Values  State 3, where the task returns the values of the time and position lists to the computer using a for loop
    @param S4_Do_Nothing  State 4  Do nothing after data has been returned
    
    '''
    S0_init=0
    S1_Take_Data=1
    S2_Stop_Check=2
    S3_Return_Values=3
    S4_Do_Nothing=4
  
    def __init__(self,myenc,interval1,myuart,t2ch1):
        '''
        @brief            Creates an EncoderRunner object.
        @param myenc   An encoder object that is updated by this object
        @param interval1  An integer that declares the delay time in microseconds between runs 
        @param start_time  An integer timestamp for the first iteration
        @param next_time1  An integer timestamp for when the next run should be
        '''
  
        ## An encoder object that is to be run by this object
        self.myenc=myenc
        
        ## An integer that declares the delay time in microseconds between runs of task1
        self.interval1=interval1
        
        
        # The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        # print('Start time:'+str(self.start_time))
        
        # The "timestamp" for when the next run should be
        self.next_time1 = utime.ticks_add(self.start_time, self.interval1)

        #Initial state of the FSM
        self.state=self.S0_init
        
        #Passed UART for communication with the computer from main.py
        self.myuart=myuart
       
        # Used for PWM in order to blink the LED
        self.t2ch1=t2ch1
        
    def run(self):
        '''
        @brief   Runs the encoder's FSM based on the interval passed from the main.py
        '''
        
        self.curr_time = utime.ticks_ms() #current time
        if utime.ticks_diff(self.curr_time,self.next_time1)>0: #runs the task if current time is past the next time
            
            # self.myuart.write(str(self.state))
        
        
        
            if(self.state == self.S0_init):
                # Run State 0 Code, initialize 
                self.time=[] #initialized time list
                self.position=[] #initialized position list
                
                self.transitionTo(self.S1_Take_Data) #transition to state 1
                 
             
            elif(self.state == self.S1_Take_Data):
                # Run State 1 Code
                self.myenc.update() #update encoder position
                self.time.append(self.curr_time-self.start_time) #add current time to time list
                self.position.append(self.myenc.get_position()) #add current encoder position to position list
                # self.myuart.write('State1, taking data')
                self.t2ch1.pulse_width_percent(100) #if taking data, turn LED to full brightness
                self.transitionTo(self.S2_Stop_Check) #transition to state 2
                
            elif(self.state == self.S2_Stop_Check):
                self.t2ch1.pulse_width_percent(0) #turn off LED if checking for stop
                if self.myuart.any() != 0: #if there's any input waiting from the computer, store inside of val
                    val = self.myuart.readchar()
                else: #otherwise set val equal to 0 so that the below if statement can be run
                    val=0
                    
                if (val==50 or self.curr_time-self.start_time>=10000): #transition to state 3 if user inputs 2 or if data collection has gone on for 10 seconds
                        self.transitionTo(self.S3_Return_Values)
                        
                else: #otherwise go back to taking data
                    self.transitionTo(self.S1_Take_Data)
                    
                    
    
            elif(self.state==self.S3_Return_Values): 
                self.myuart.write('{:}\r\n'.format(len(self.position))) #passes length of list back to main in order to control loop
                for n in range(len(self.position)): #loops through entirety of time and position lists
                    self.myuart.write('{:},{:}\r\n'.format(self.time[n],self.position[n])) #write values of time and position seperated by a comma at current index
                                      
                self.transitionTo(self.S4_Do_Nothing) #after all values are returned, do nothing
                                          
            elif(self.state==self.S4_Do_Nothing):
                print('S4 do nothing') #proof that end has been reached
                self.t2ch1.pulse_width_percent(80) #proof that end has been reached
             
            self.next_time1 = utime.ticks_add(self.next_time1, self.interval1) #set the next time for the task to actually run
            
   
    def transitionTo(self,newstate): 
        '''
        @brief This function transitions the state variable to reflect the current state inside the FSM.
        
        '''
        self.state=newstate 
