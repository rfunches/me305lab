'''
@file UI_Front.py

This file contains the UI Front end to be run on the computer for data collection with
an encoder. The user is able to begin data collection by inputting a "g", which writes a signal
through the USB cable to the Nucleo. The program then checks for user input to tell the 
Nucleo to stop data collection. Next, the time and position lists are sent to the computer one at a time
and stored in local lists. Finally, the data is plotted for user visualization.

'''
import array
import time
import serial
import numpy
import matplotlib


ser = serial.Serial(port='COM3',baudrate=115273,timeout=1) #initializes serial port for communication with Nucelo


print('Welcome to the UI for Encoder Interaction')
print('Type \'G\' to begin data collection, and press \'S\' to end data collection')

n=0
while(n==0):
    inv = input('Give me a character: ') #takes input from user
    if(inv=='G' or inv=='g'): #if the user entered g or G, get out of loop
        n=1
    else: #if g or G not entered, tell user to try again
        print('You need to input \'G\' in order to collect data.')

ser.write('1'.encode('ascii')) #send a 1 to the Nucleo, which signifies data collection to start

secondcheck='a' #initializes the variable that will store user input to stop data collection
timing=time.time() #start time for the stop loop to run
      
while(secondcheck!='s' and secondcheck!='S' and time.time()<=timing+10): #loop ends if user inputs s,S, or time reaches 10 seconds
   
    secondcheck=input('Enter the letter \'S\' to complete data collection, otherwise automatic stop after 10 seconds            ') #userpropmpt
    if (secondcheck=='s' or secondcheck=='S'): #if the user input is to stop, send 2 to Nucleo, telling board to stop collecting data
        ser.write('2'.encode('ascii'))
        
t=[] #initialize time list
position=[]  #initialize position list
time.sleep(3) #stall for 3 seconds to ensure that all variables put into UART on Nucleo side
poslength=int(ser.readline()) #first thing passed to main is the length of list that will be passed, used for loop control

for n in range(1,poslength): #loop through entire list
    line_string = ser.readline().decode('ascii') #read the line which has time and position comma seperated
    line_string=(line_string.strip()) #strips all special characters from String, such as \r and \n
    print(line_string)								# print the line to see in command prompt
    line_list = line_string.split(',')		#split string at comma, 0th entry is time, 1st entry is position
    t.append(int(line_list[0]))				#append 0th entry to t list	
    position.append(int(line_list[1]))		#append 1st entry to position list



#plotting
matplotlib.pyplot.plot(t,position)	#plot command
matplotlib.pyplot.xlabel('Time (ms)') #x-axis label
matplotlib.pyplot.ylabel('Counts (units)') #y-axis label, can additionally convert to degrees if needed

#save as CSV file
numpy.savetxt('dataoutput.csv',list(zip(t,position)),delimiter=',') #output csv file from 2D list


