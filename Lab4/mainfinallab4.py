'''
@file mainfinallab4.py

This file contains the main method that runs on the Nucleo upon start up. This file initializes Pin A5 
on the board to blink an LED in order to troubleshoot the program, as well as an encoder object and
encoder runner object. Once the user commands the Nucleo to begin data collection, the Encoder Controller runs at a fixed rate as 
defined by interval1.

'''
import pyb
from Encoder import Encoder
from EncoderRunner1 import EncoderRunner
from pyb import UART
import utime

pinA5 = pyb.Pin (pyb.Pin.cpu.A5) #Initialize Pin that blinks LED for troubleshooting
tim2 = pyb.Timer(2, freq = 20000) #Initialize timer for LED
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5) #Initilize pulse width modulation for LED
        

period=0xffff #Period of timer

tim=pyb.Timer(3) #Declare timer 3
tim.init(prescaler=0,period=0xFFFF) #Initialize timer 3 with prescale and period
tim.channel(1,pin=pyb.Pin.cpu.A6,mode=pyb.Timer.ENC_AB) #channel 1 of timer 3 to pin A6
tim.channel(2,pin=pyb.Pin.cpu.A7,mode=pyb.Timer.ENC_AB) #chennel 2 of timer 3 to pin A7


myenc=Encoder(tim,period) #declare encoder object, pass timer and period

myuart=pyb.UART(2) #initialize UART for communication with computer
interval1=100 #Interval at which FSM will run tasks in ms

n=0 #loop control variable
while(n==0): #runs while n==0, changed from 0 when computer sends 1
    if myuart.any() != 0: #if there's anything from the computer
        val = myuart.readchar() #read the character
        if val==49: #if the sent value is a 1
            n=1 #break out of the loop
    
EncControl=EncoderRunner(myenc, interval1,myuart,t2ch1) #Declare EncoderRunner to control timing of Encoder updates

while(True): #infinite loop
    EncControl.run() #run the Encoder Controller

    

