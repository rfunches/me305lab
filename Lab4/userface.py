'''
@file userface.py

This file contains the user interface for the Encoder lab. After offering user instructions
in the constructor, the run() method looks for input from the keyboard using the UART class, and accesses
shared variables stored in shares.py in order to allow user manipulation of stored variables.
'''

import pyb
import utime
import shares


class UserInterface:
    
    def __init__(self,interval2):
        '''
        @brief This method constructs a user interface object and delegates user interface commands to the user
        @param ser  This UART variable passes characters from the keyboard to this program
        @param interval2  An integer that declares the delay time in microseconds between runs 
        @param start_time2  An integer timestamp for the first iteration
        @param next_time2  An integer timestamp for when the next run should be
        
        '''
           
        self.ser=pyb.UART(2)
        # print('I got to step 1')
        
        ## An integer that declares the delay time in microseconds between runs of task1
        self.interval2=interval2
        
        
        # The timestamp for the first iteration
        self.start_time2 = utime.ticks_ms()
        # print('Start time:'+str(self.start_time))
        
        # The "timestamp" for when task 1 should run next
        self.next_time2 = utime.ticks_add(self.start_time2, self.interval2)
        # print('Next Time1:'+str(self.next_time1))
        
        
        #User interface commands to the user
        print('Welcome to the Encoder Reader Program, here we offer use of a short list of commands, listed below')
        print('If you want to zero the encoder position, type \"Z\"') 
        print('If you want to print out the encoder position, type \"P\"')
        print('If you want to print out the encoder delta, type \"D\"')
       
    def run(self):
        '''
        @brief This method checks for user input at a standard rate as given by interval. The three tasks that
        this function will perform is either printing delta or position from shares.py, or communicating to the encoder to zero the position
        by way of shares.py.
        
        '''
        # print('I got to step 2')
        self.curr_time = utime.ticks_ms() #current time
        # print('Current time1:'+str(self.curr_time))
        if utime.ticks_diff(self.curr_time,self.next_time2)>0: #runs the task if current time is past the next time
            if(self.ser.any()>0): #if there are any characters waiting
                intake=self.ser.readchar() #the char value of the character is stored in intake
                if(intake==100 or intake==68): #if intake is the char value of 'd' or 'D'
                    print(str(shares.delta)) #print current delta from shares.py
                if(intake==80 or intake==112): #if intake is the char value of 'p' or 'P'
                    print(str(shares.position)) #print current position from shares.py
                if(intake==122 or intake==90): #if intake is the char value of 'z' or 'Z'
                    shares.clearpos=1 #set clearpos inside of shares.py to 1, which signifies to encoder.py to clear position
                else: #if not one of the above commands but there is a character
                    print('Not a valid character') #tell user invalid
                
            
            
            