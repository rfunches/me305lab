'''
@file ElevatorFSM.py

This file containst the classes for TaskElevator, Button, Sensor, and MotorDriver.
These objects are used in conjunction with the main.py in order to run a FSM that 
simulates an elevator moving up and down between two floors with random inputs 
to the buttons. 
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control an elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1
    S1_MOVING_DOWN  = 1    
    
    ## Constant defining State 2
    S2_MOVING_UP = 2    
    
    ## Constant defining State 3
    S3_STOPPED_AT_1      = 3    
    
    ## Constant defining State 4
    S4_STOPPED_AT_2     = 4
    
    ##
    S5_DO_NOTHING=5
    
    
    def __init__(self, interval, Button1, Button2, motor, first, second,secondpos):
        '''
        @brief            Creates a TaskElevator object.
        @param Button1   An object from class Button representing on/off for the first floor request button
        @param Button2   An object from class Button representing on/off for the second floor request button
        @param Motor      An object from class MotorDriver representing a DC motor.
        @param first    An object from class Sensor representing on/off for the first floor sensor 
        @param second   An object from class Sensor representing on/off for the second floor sensor 
        @param initialheight   An integer that dictates the initial height of the elevator
        @param interval    A float that determines the time interval at which the tasks will run 
        
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The Button1 object initialized and cleared
        self.Button1 = Button1
        Button1.setButtonState(0)
        
        ## The Button2 object initialized and cleared
        self.Button2=Button2
        Button2.setButtonState(0)
                
        ## The motor object raising the elevator
        self.motor = motor
        
        ##A Sensor object that will determine if the elevator is at floor 1
        self.first=first
        
        ##Sensor object that will determine if the elevator is at floor 2
        self.second=second
        
        ##Integer that is 1 if the elevator starts at floor 2, 0 otherwise
        self.initialheight=secondpos
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        
        '''
        @brief      Runs one iteration of the task
        '''
        
        
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
             # Run State 0 Code
                 self.transitionTo(self.S1_MOVING_DOWN)
                 if(self.initialheight==1):
                     self.motor.Down()
                 print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
             
            elif(self.state == self.S1_MOVING_DOWN):
                # Run State 1 Code
                if(self.first.getSensorState()==1):
                    self.transitionTo(self.S3_STOPPED_AT_1)
                    self.motor.Stop()
                    self.Button1.setButtonState(0)
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
             
            elif(self.state == self.S2_MOVING_UP):
             # Run State 2 Code
                if(self.second.getSensorState()==1):
                    self.transitionTo(self.S4_STOPPED_AT_2)
                    self.motor.Stop()
                    self.Button2.setButtonState(0)
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
             
            elif(self.state == self.S3_STOPPED_AT_1):
             # Transition to state 1 if the left limit switch is active
                 if (self.Button1.getButtonState()==1):
                     self.Button1.setButtonState(0)
                 if(self.Button2.getButtonState()==1):
                     self.transitionTo(self.S2_MOVING_UP)
                     self.motor.Up()
                 print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
             
            elif(self.state == self.S4_STOPPED_AT_2):
             # Run State 4 Code
                 if (self.Button2.getButtonState()==1):
                     self.Button2.setButtonState(0)
                 if(self.Button1.getButtonState()==1):
                     self.transitionTo(self.S1_MOVING_DOWN)
                     self.motor.Down()
                 print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
             
           
            self.runs += 1
             
             # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that can be pushed by the
                imaginary elevator passengers in order to request a floor to move to. 
                As of right now this class is implemented using "pseudo-hardware". 
                That is, we are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin,floorchoice):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        ##
        self.floorchoice=floorchoice
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return self.floorchoice
    
    def setButtonState(self,floorchoice):
         '''
        @brief      Sets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized request for the button to be on or off.
        @return     A boolean representing the state of the button.
        '''
         self.floorchoice=floorchoice

        
class Sensor:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin,position):
        '''
        @brief      Creates a Sensor object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        ##
        self.position=position
        
        print('Sensor object created attached to pin '+ str(self.pin))

    
    def getSensorState(self):
        '''
        @brief      Gets the sensor state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([0,1])
    
  
class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the elevator
                translate up and down.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Up(self):
        '''
        @brief Moves the motor forward
        '''
        print('Elevator moving up')
    
    def Down(self):
        '''
        @brief Moves the motor forward
        '''
        print('Elevator moving down')
    
    def Stop(self):
        '''
        @brief Moves the motor forward
        '''
        print('Elevator stopped')



























