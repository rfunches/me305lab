'''
@file mainelevator.py
'''

from ElevatorFSM import Button, MotorDriver, TaskElevator,Sensor
from random import choice

        
# Creating objects to pass into task constructor
Button_1 = Button('PB6',1)
Button_2 = Button('PB7',1)
Motor = MotorDriver()

firstpos=0
secondpos=1-firstpos
first=Sensor('PB8',firstpos)
second=Sensor('PB9',secondpos)

# Creating a task object using the button and motor objects above
task1 = TaskElevator(1, Button_1, Button_2, Motor, first, second,secondpos)
task2 = TaskElevator(1, Button_1, Button_2, Motor, first, second,secondpos)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
    task1.Button1.setButtonState(choice([0,1]))
    task1.Button2.setButtonState(choice([0,1]))
    task2.run()
    task2.Button1.setButtonState(choice([0,1]))
    task2.Button2.setButtonState(choice([0,1]))